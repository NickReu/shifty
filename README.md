# Shifty [<_<]
[<_<] (aka Shifty) is a Weaver Dice bot. It handles dice rolling and wound rolling, and has a few other features. Shifty is a trimmed-down version of Smiley, which is a fully-featured bot with much more functionality that runs on the official Parahumans Discord.

## Inviting Shifty
[Click here](https://discord.com/oauth2/authorize?client_id=885168029560877066&scope=bot&permissions=68608)

[You'll also need to click here](https://discord.com/api/oauth2/authorize?client_id=885168029560877066&scope=applications.commands) if you want to allow `/` commands.

## Using Shifty
Type `%help` to see the list of commands. Type `%help <command>` to get more information about a specific command.

For example: `%help roll`

Run a command by typing `%` (the percent sign) and then the name of the command,
or use commands with the new application command interaction by typing `/` instead.

Application commands ('slash commands') can be easier to use, since Discord will show you what options are available and allowed, and valid inputs for them.

For example: `%roll` or `/roll`

Some commands take arguments, which are more information after the name of the command.

For example: `%wound3 moderate bash` or `/wound3 sev:moderate wtype:bash`

Don't worry that the slash command has more typing - it also supports autocomplete so you don't need to type everything out.
